howto: |
  **Objective:** *Fill at least 75% of the field and advance to the next level.*
  
  The player is presented a rectangular tiled field surrounded by wall. Two or more balls move about in the field and bounce off of the walls. The object of the game is to limit the size of room in which the balls are.

  The size of the active area of the field is decreased by making new walls that enclose areas without balls in them. To complete a level, the player must decrease the size of the active field by at least 75% within the time allowed.
  
  New walls are built by clicking the left mouse button in an active area of the field, at which point two walls will begin to grow in opposite directions from the square the mouse was clicked in. Only one wall in each direction may be growing on the screen at any given time.

  When the mouse is on the field, the cursor is shown as a pair of arrows pointing in opposite directions, either horizontally or vertically. The arrows point in the direction the walls will grow when the **left mouse button** is clicked. This direction can be changed by clicking the **right mouse button**.

  A new wall has a **"head"** which moves away from the point where the mouse was clicked. A wall is not permanent until this **"head"** runs into another wall. If a ball collides with any part of the wall except the head, before the head has run into another wall, the new wall will disappear completely and one life will be lost. If a ball collides with the head in the direction of the wall's growth, the wall will stop growing there, and become permanent, with no loss of life. If a ball collides with the head from any other side, the ball will bounce off and the wall will continue to grow normally.
