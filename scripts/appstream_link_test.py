import re
import unittest

from bs4 import BeautifulSoup


class AppStreamLinkTestCase(unittest.TestCase):
    def test_appstream_link(self):
        with open('public/dolphin/index.html') as fp:
            soup = BeautifulSoup(fp, 'html.parser')
            if tag := soup.find('a', href=re.compile('^appstream://')):
                self.assertEqual(tag.get('href'), 'appstream://org.kde.dolphin.desktop')
        with open('public/skrooge/index.html') as fp:
            soup = BeautifulSoup(fp, 'html.parser')
            if tag := soup.find('a', href=re.compile('^appstream://')):
                self.assertEqual(tag.get('href'), 'appstream://org.kde.skrooge')


if __name__ == '__main__':
    unittest.main()
